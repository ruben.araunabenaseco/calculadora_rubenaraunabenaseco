/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora_rubénaraunabeñaseco;

import java.util.Scanner;

/**
 *
 * @author DAM102
 */
public class CalculadoraExamen {
    
    static Scanner scanner = new Scanner(System.in);
    static int opcion = -1;
    static double numero1 = 0, numero2 = 0;
    
    public static void main(String[] args) {
        
        while (opcion != 0) {
            try {
                System.out.println("Elige opción:\n" + ""
                        + "1. -Sumar\n"
                        + "2. -Restar\n"
                        + "3. -Multiplicar\n"
                        + "4. -Dividir\n"
                        + "0. -Salir\n");
                
                System.out.println("Selecciona una opción de 0 a 4");
                opcion = Integer.parseInt(scanner.nextLine());
                
                switch (opcion) {
                    case 1:
                        pideNumeros();
                        System.out.println(numero1 + " + " + numero2 + " = " + (numero1 + numero2));
                        break;
                    case 2:
                        pideNumeros();
                        System.out.println(numero1 + " - " + numero2 + " = " + (numero1 - numero2));
                        break;
                    case 3:
                        pideNumeros();
                        System.out.println(numero1 + " * " + numero2 + " = " + (numero1 * numero2));
                        break;
                    case 4:
                        pideNumeros();
                        System.out.println(numero1 + " / " + numero2 + " = " + (numero1 / numero2));
                        break;
                    case 0:
                        System.out.println("Saliendo...");
                        break;
                    default:
                        System.out.println("Opción no disponible");
                        break;
                }
                
                System.out.println("\n");
            } catch (Exception e) {
                System.out.println("Error!");
            }
        }
    }
    
    public static void pideNumeros() {
        System.out.println("Introduce el primer número:");
        numero1 = Double.parseDouble(scanner.nextLine());
        
        System.out.println("Introduce el segundo número:");
        numero2 = Double.parseDouble(scanner.nextLine());
    }
    
}
